//rendu des données 

//tr = ligne
//th = header
//td = cellule

var table = null;

function createTableBooks(books) {
    table = document.createElement("table");
    $(CONTENT).append(table);
    $(table).append(getHeaderLineForTableBook());
    getContentForTableBook(books);
    $(table).addClass("col-xs-10 col-xs-offset-1 color1 books");
}

function getContentForTableBook(books) {
    $.each(books, function () {
        $(table).append(getLineForTableBook(this));
    });
}

function getLineForTableBook(book) {
    var ligneBook = document.createElement("tr");
    var idBook = document.createElement("td");
    var titleBook = document.createElement("td");
    var authorName = document.createElement("td");

//équivalent a ce qui est en dessous
//    $(ligneBook).append($(idBook).text(book.id));

    $(idBook).text(book.id).addClass("col-xs-1");
    $(titleBook).text(book.title).addClass("col-xs-8");
    $(authorName).text(book.author).addClass("col-xs-3");

    $(ligneBook).append(idBook);
    $(ligneBook).append(titleBook);
    $(ligneBook).append(authorName);

    $(ligneBook).click(function (){
        alert(book.id);
        getCopiesFromBook(book.id);
    });

    return ligneBook;

//    ("id : " + book.id + "- titre :" + book.title + "- auteur :" + book.author);

}

function getHeaderLineForTableBook() {
    //creation des balises
    var ligne = document.createElement("tr");
    var idBook = document.createElement("th");
    var titleBook = document.createElement("th");
    var authorName = document.createElement("th");

    //affectation des textes
    $(idBook).text("id");
    $(titleBook).text("Titre");
    $(authorName).text("Auteur");

    //insertion des th dans la ligne
    $(ligne).append(idBook);
    $(ligne).append(titleBook);
    $(ligne).append(authorName);

    return ligne;
}

function createTableAuthor(authors) {
    table = document.createElement("table");
    $(CONTENT).append(table);
    $(table).append(getHeaderLineForTableAuthor());
    getContentForTableAuthor(authors);
    $(table).addClass("col-xs-10 col-xs-offset-1 color1");
}

function getContentForTableAuthor(authors) {
    $.each(authors, function () {
        $(table).append(getLineForTableAuthor(this));
    });
}

function getLineForTableAuthor(author) {
    var ligneAuthor = document.createElement("tr");
    var idAuthor = document.createElement("td");
    var firstnameAuthor = document.createElement("td");
    var nameAuthor = document.createElement("td");

    $(idAuthor).text(author.id);
    $(firstnameAuthor).text(author.nom);
    $(nameAuthor).text(author.prenom);

    $(ligneAuthor).append(idAuthor);
    $(ligneAuthor).append(firstnameAuthor);
    $(ligneAuthor).append(nameAuthor);

    return ligneAuthor;

//    ("id : " + book.id + "- titre :" + book.title + "- auteur :" + book.author);

}

function getHeaderLineForTableAuthor() {
    //creation des balises
    var ligne = document.createElement("tr");
    var idAuthor = document.createElement("th");
    var firstnameAuthor = document.createElement("th");
    var authorName = document.createElement("th");

    //affectation des textes
    $(idAuthor).text("id");
    $(firstnameAuthor).text("Nom");
    $(authorName).text("Prénom");

    //insertion des th dans la ligne
    $(ligne).append(idAuthor);
    $(ligne).append(firstnameAuthor);
    $(ligne).append(authorName);

    return ligne;
}


/*COPIES*/
function createTableCopies(copies) {
    if(listOfStates === null){
    getStates();        
    }
    
    table = document.createElement("table");
    $(CONTENT).append(table);
    $(table).append(getHeaderLineForTableCopy());
    getContentForTableCopies(copies);
    $(table).addClass("col-xs-10 col-xs-offset-1 color1");
}

function getContentForTableCopies(copies) {
    $.each(copies, function () {
        $(table).append(getLineForTableCopy(this));
    });
}


function getSelect(list,selectedId){
    var select = document.createElement("select");
    $.each(list,function(){
       var opt = document.createElement("option");
       $(opt).val(this.id).text(this.name);
       if (selectedId !== null){
           if (this.id === selectedId){
               $(opt).attr("selected","selected");
           }
       }
       $(select).append(opt);
    });
    
    return select;
}


function getLineForTableCopy(copy) {
    var ligneCopy = document.createElement("tr");
    var idCopy = document.createElement("td");
    var stateCopy = document.createElement("td");
    var statusCopy = document.createElement("td");
    var priceCopy = document.createElement("td");
    var ctrlCopy = document.createElement("td");

//équivalent a ce qui est en dessous
//    $(ligneBook).append($(idBook).text(book.id));

    $(idCopy).text(copy.id).addClass("col-xs-1");
    
    var selectState = getSelect(listOfStates,copy.state.id);
    
    $(selectState).change(function(){
        alert('okokokok');
       updateStateOfCopy(copy.id,$(this).val()); 
    });
    
    $(stateCopy).append(selectState).addClass("col-xs-5");
    $(statusCopy).text(copy.status).addClass("col-xs-3");
    $(priceCopy).text(copy.price).addClass("col-xs-3");
    $(ctrlCopy).text("").addClass("col-xs-3");

    $(ligneCopy).append(idCopy);
    $(ligneCopy).append(stateCopy);
    $(ligneCopy).append(statusCopy);
    $(ligneCopy).append(priceCopy);
    $(ligneCopy).append(ctrlCopy);

    return ligneCopy;


}

function getHeaderLineForTableCopy() {
    //creation des balises
    var ligne = document.createElement("tr");
    var cellId = document.createElement("th");
    var cellState = document.createElement("th");
    var cellStatus = document.createElement("th");
    var cellPrice = document.createElement("th");
    var cellCtrl = document.createElement("th");

    //affectation des textes
    $(cellId).text("id");
    $(cellState).text("Etat");
    $(cellStatus).text("Statut");
    $(cellPrice).text("Prix");
    $(cellCtrl).text("Options");

    //insertion des th dans la ligne
    $(ligne).append(cellId);
    $(ligne).append(cellState);
    $(ligne).append(cellStatus);
    $(ligne).append(cellPrice);
    $(ligne).append(cellCtrl);

    return ligne;
}