/* 
 * Requete ajax de type GET
 */



function getBooks() {

    $.ajax({
        url: URL + "consulting/books",
        type: 'GET',
        dataType: 'json',
        async: true,
        success: function (datas) {
                $(CONTENT).empty();
            createTableBooks(datas);
            },
            error: function (){
                alert("wtf");
        }
    });
};
function getAuthors() {
    $(CONTENT).empty();

    $.ajax({
        url: URL + "consulting/author",
        type: 'GET',
        dataType: 'json',
        async: true,
        success: function (datas) {
            createTableAuthor(datas);
            },
            error: function (){
                alert("wtf");
        }
    });
};
function getCopiesFromBook(id){
        $(CONTENT).empty();

    $.ajax({
        url: URL + "consulting/books/"+id+"/copies",
        type: 'GET',
        async: true,
        dataType:'json',
        success: function (copies){
            createTableCopies(copies);
        },
        error: function (){
            alert("wtf error");
        }
    });
};
function getStates(){
    $.ajax({
        url: URL + "consulting/states",
        type: 'GET',
        async: false,
        dataType:'json',
        success: function (states){
            listOfStates = states;
        },
        error: function (){
            alert("wtf error");
        }
    });
};
function updateStateOfCopy(id,value){
    alert("LAA");
    $.ajax({
        url: URL + "administration/copies/"+id+"/state",
        type: 'PUT',
        async: true,
        dataType:'json',
        data:{"stateId" : value},
        success: function (copy){
            alert(copy);
        },
        error: function (){
            alert("wtf error");
        }
    });
};